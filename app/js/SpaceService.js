(function () {
    "use strict";
    angular
        .module('reservationApp.service', [])
        .factory('SpaceService', SpaceService);

    function SpaceService() {

        //sparqlRequest();

        var SpaceService = {
            allSpaces: generateSpaces(),
            getSpacesWithEquipment: getSpacesWithEquipment,
            getSpacesWithCapacity: getSpacesWithCapacity,
            getYousense: getYousense
        };

        return SpaceService;

        function generateSpaces() {
            var kino = {
                rating: 0,
                name: 'Kino',
                people: 8,
                equipment: ['projector', 'wireless', 'videoconf', 'whiteboard'],
                imgsrc: 'images/kino.jpg',
                yousense: "Now in kino I am sitting. It is dry air and low frequency humming. It is probably because of ac makes noise. It's bearable here!"
            };

            var engine_room = {
                rating: 0,
                name: 'Engine room',
                people: 20,
                equipment: ['projector', 'wireless', 'whiteboard'],
                imgsrc: 'images/df.jpg'
            };
            var stage = {
                name: 'The Stage',
                people: 150,
                equipment: ['projector', 'wireless', 'whiteboard', 'microphone', 'lighting', 'videoconf'],
                imgsrc: 'images/stage.jpg',
                yousense: "Now in the stage I am looking around. It is motivating and good. It is probably because of silence. I like it here! The space is huge. Now in the stage I am streaming. It is cozy. It is probably because of nice space. I like it here! The space has doors.",
                rating: 2
            };

            var studio = {
                name: 'The Studio',
                people: 50,
                equipment: ['projector', 'audio'],
                imgsrc: 'images/df.jpg',
                yousense: "Now in the studio I am sitting and studying. It is interesting, relaxing, excellent and bright. It is probably because of seminar started. I like it here! The space has doors. Now in studio I am discussing and listening. It is fresh air. It is probably because of nice 3rd session. I like it here! Now in the studio I am discussing and on a lecture. It is dim. It is probably because of we have the projectors on. I like it here! Now in the studio I am working. It is interesting, very good and tired. It is probably because of i am getting sick, nice space and people around. I like it here! The space is very inspiring but also challenging.",
                rating: 5
            };

            var audition = {
                rating: 0,
                name: 'The Audition',
                people: 10,
                equipment: ['projector', 'videoconf', 'speakers', 'wireless'],
                imgsrc: 'images/df.jpg'
            };

            var olokolo = {
                rating: 0,
                name: 'Olokolo',
                people: 6,
                equipment: ['wireless'],
                imgsrc: 'images/df.jpg'
            };
            return [kino, engine_room, stage, studio, audition, olokolo];
        }

        function getSpacesWithEquipment(equipment, spacesArg) {
            var spaces = spacesArg ? spacesArg : this.allSpaces;
            var result = [];
            var spaceEquipment;
            for (var i = 0; i < spaces.length; i++) {
                spaceEquipment = spaces[i].equipment;
                for (var j = 0; j < spaceEquipment.length; j++) {
                    if (equipment === spaceEquipment[j]) {
                        result.push(spaces[i]);
                        break;
                    }
                }
            }
            return result;
        }

        function getSpacesWithCapacity(capacity, spacesArg) {
            var spaces = spacesArg ? spacesArg : this.allSpaces;
            var result = [];
            for (var i = 0; i < spaces.length; i++) {
                if (capacity <= spaces[i].people) {
                    result.push(spaces[i]);
                    continue;
                }
            }
            return result;
        }
    }

    function getYousense(string, spacesArg) {
        var spaces = spacesArg ? spacesArg : this.allSpaces;
        console.log(string);
        var result = [];
        for (var i = 0; i < spaces.length; i++) {
            if (!!spaces[i].yousense && spaces[i].yousense.match(new RegExp(string,'g'))) {
                result.push(spaces[i]);
            }
        }
        return result;
    }

    function sparqlRequest() {
        var sparqler = new SPARQL.Service('http://yousense.aalto.fi/d2rq/sparql');


        //parqler.addDefaultGraph("http://thefigtrees.net/lee/ldf-card");
        //sparqler.addNamedGraph("http://torrez.us/elias/foaf.rdf");
        sparqler.setPrefix("rdfs", "http://www.w3.org/2000/01/rdf-schema#");
        sparqler.setPrefix("db", "http://yousense.aalto.fi/d2rq/resource/");
        sparqler.setPrefix("geo", "http://www.w3.org/2003/01/geo/wgs84_pos#");
        sparqler.setPrefix("owl", "http://www.w3.org/2002/07/owl#");
        sparqler.setPrefix("map", "http://yousense.aalto.fi/d2rq/resource/#");
        sparqler.setPrefix("xsd", "http://www.w3.org/2001/XMLSchema#");
        sparqler.setPrefix("rdf", "http://www.w3.org/1999/02/22-rdf-syntax-ns#");
        sparqler.setPrefix("experience", "http://linkedearth.org/experience/ns/");
        sparqler.setPrefix("vocab", "http://yousense.aalto.fi/d2rq/resource/vocab/");
        sparqler.setPrefix("dcterms", "http://purl.org/dc/terms/");

// "json" is the default output format
        sparqler.setOutput("json");

        var query = sparqler.createQuery();
        query.query("SELECT ?message ?timestamp ?positiveness ?location WHERE {\n"+
            "?feedback dcterms:description ?message.\n"+
            "?feedback a experience:Experience.\n" +
            "?feedback vocab:isPositive ?positiveness.\n"+
            "?feedback vocab:isTesting 0.\n" +
            "?feedback experience:hasLocation ?location.\n"+
     "# FILTERS OUR ENTRIES USED FOR TESTING\n" +
            "?feedback experience:hasTime ?timestamp\n"+
    "}", {failure: onFailure,
            success: onSuccess});

        function onFailure() {
            console.log("Failed!");
        }

        function onSuccess(values) {
            console.log("values");
        }
    }
    /*
     PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
     PREFIX db: <http://yousense.aalto.fi/d2rq/resource/>
     PREFIX geo: <http://www.w3.org/2003/01/geo/wgs84_pos#>
     PREFIX owl: <http://www.w3.org/2002/07/owl#>
     PREFIX map: <http://yousense.aalto.fi/d2rq/resource/#>
     PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
     PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
     PREFIX experience: <http://linkedearth.org/experience/ns/>
     PREFIX vocab: <http://yousense.aalto.fi/d2rq/resource/vocab/>
     PREFIX dcterms: <http://purl.org/dc/terms/>
     SELECT ?message ?timestamp ?positiveness ?location WHERE {
     ?feedback dcterms:description ?message.
     ?feedback a experience:Experience.
     ?feedback vocab:isPositive ?positiveness.
     ?feedback vocab:isTesting 0.
     ?feedback experience:hasLocation ?location.
     # FILTERS OUR ENTRIES USED FOR TESTING
     ?feedback experience:hasTime ?timestamp
     }
     */
})();