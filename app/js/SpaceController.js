(function () {
    "use strict";
    angular
        .module('reservationApp',
        ['ui.bootstrap',
            'reservationApp.service'])
        .controller('SpaceController', SpaceController);

    SpaceController.$inject = ['$scope', 'SpaceService'];

    function SpaceController($scope, SpaceService) {
        var vm = this;

        vm.spaces = SpaceService.allSpaces;

        vm.types = getTypes();
        vm.selectedType = null;
        vm.criterium = null;
        vm.selects = [
            {
                type: null,
                criterium: null,
                yousense: false,
                yousenseString: null
            }
        ];
        vm.execute = execute;
        vm.addCriteria = addCriteria;
        vm.removeCriteria = removeCriteria;
        vm.sorts = getSorts();
        vm.sortBy = null;

        $scope.$watch(function() {
            return vm.sortBy;
        }, sort);

        function sort(newValue, oldValue) {
            if (!newValue) {
                return;
            }
            var newSpaces, i;
            console.log('here');
            if (newValue.type === 1) {
                console.log("sort here");
                vm.spaces.sort(function (a, b) {
                    return b.rating - a.rating;
                })
            }
            else if (newValue.type === 2)
            {
                console.log("sort there");
                vm.spaces.sort(function(a,b) {
                    return a.people - b.people;
                });
            }
        }

        function execute() {
            var spaces = SpaceService.allSpaces;
            var select;
            for (var i = 0; i < vm.selects.length; i++) {
                select = vm.selects[i];
                if (select.type.name === 'Equipment') {
                    spaces = SpaceService.getSpacesWithEquipment(select.criterium.name, spaces);
                }
                else if (select.type.name === 'Capacity') {
                    console.log(select.criterium.name);
                    spaces = SpaceService.getSpacesWithCapacity(select.criterium.name, spaces);
                }
                else if (select.type.name ==='YouSense') {
                    console.log("here");
                    spaces = SpaceService.getYousense(select.yousenseString, spaces);
                }
            }
            vm.spaces = spaces;
            sort(vm.sortBy);
        }

        function addCriteria() {
            vm.selects.push({
                type: null,
                criterium: null
            });
        }

        function removeCriteria() {
            vm.selects.pop();
        }

        function getTypes() {
            return [
                {
                    name: 'Capacity',
                    yousense: false,
                    criteria: [
                        {
                          name: 5,
                            desc: '<5'
                        },
                        {
                            name: 10,
                            desc: '<10'
                        },
                        {
                            name: 20,
                            desc: '<20'
                        },
                        {
                            name: 30,
                            desc: '<30'
                        },
                        {
                            name: 40,
                            desc: '<40'
                        },
                        {
                            name: 50,
                            desc: '<50'
                        },
                        {
                            name: 51,
                            desc: '>50'
                        }
                    ]
                },
                {
                    name: 'Equipment',
                    yousense: false,
                    criteria: [
                        {
                            name: 'projector',
                            desc: 'Projector'
                        },
                        {
                            name: 'videoconf',
                            desc: 'Video conferencing'
                        },
                        {
                            name: 'wireless',
                            desc: 'Wireless'
                        },
                        {
                            name: 'microphone',
                            desc: 'Microphone'
                        },
                        {
                            name: 'lighting',
                            desc: 'Adjustable lighting'
                        },
                        {
                            name: 'audio',
                            desc: 'Audio equipment'
                        },
                        {
                            name: "whiteboard",
                            desc: "Whiteboards"
                        }
                    ]
                },
                {
                    name: 'YouSense',
                    yousense: 'true'
                }];
        }

        function getSorts() {
            var sorts = [
                {name: "By Yousense rating",
                type: 1},
                {name: "By capacity, smallest first",
                type: 2}
            ];
            return sorts;
        }
    }
})();